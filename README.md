This is a case for the BTT SKR Mini E3 and the associated TFT35-E3 V3.0
which are originally intended as exact physical fit upgrades for the
Ender 3 and related printers, to use them on other printers with 20mm
pitch v-slot or t-slot aluminum extrusion frames.

Built in SolidWorks 2019, exported in x_t, STEP and STL.

More information is included in the associated [build log](https://forum.makerforums.info/t/cantilever-printer-design-based-on-leftovers/79951)

Created by Eric Lien and Michael K Johnson
License: [CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/)
